create TABLE phone (
    id SERIAL PRIMARY KEY,
    number VARCHAR(255),
    text VARCHAR(255),
    date VARCHAR(255));
