const db = require('../db');

class PhoneController {
    async createPhone(req, res) {
        const { id, number, text, date } = req.body;
        console.log(id, number, text, date);
        const newPhone = await db.query(
            `INSERT INTO phone (id, number,text,date) values ($1, $2, $3, $4) RETURNING *`,
            [id, number, text, date]
        );
        res.json(newPhone.rows);
    }
    async getPhones(req, res) {
        const phones = await db.query('SELECT * FROM phone');
        res.json(phones.rows);
    }
    async getOnePhone(req, res) {
        const id = req.params.id;
        const phones = await db.query('SELECT * FROM phone where id = $1', [
            id,
        ]);
        res.json(phones.rows);
    }
    async updatePhone(req, res) {
        const { id, number, text, date } = req.body;
        const phones = await db.query(
            'UPDATE phone set number = $1, text = $2, date = $3 where id = $4 RETURNING *',
            [number, text, date, id]
        );
        res.json(phones.rows);
    }
    async deletePhone(req, res) {
        const id = req.params.id;
        const phones = await db.query('DELETE FROM phone where id = $1', [id]);
        res.json(phones.rows);
    }
}

module.exports = new PhoneController();
