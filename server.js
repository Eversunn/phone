const express = require('express');
const { createProxyMiddleware } = require('http-proxy-middleware');
const path = require('path');
const phoneRouter = require('./routes/phone.routes');

const PORT = process.env.PORT || 4200;

const app = express();
app.set('view engine', 'ejs');
app.use(express.json());
app.use(
    '/api',
    phoneRouter,
    createProxyMiddleware({
        target: 'localhost:4200',
        changeOrigin: true,
    })
);
app.use;
app.use('/js', express.static(path.join(__dirname, '/js')));
app.use('/css', express.static(path.join(__dirname, '/css')));

app.get('/', (req, res) => {
    res.render('index');
});

app.listen(PORT, () => console.log(`listening on port ${PORT}`));
