///Html///
const table = document.getElementById('tableBody');

///modalWindow///
let isEdit = false;

const formID = document.getElementById('formID');
const formNum = document.getElementById('formNum');
const formText = document.getElementById('formText');
const formDate = document.getElementById('formDate');

const modal = document.getElementById('myModal');
const btn = document.getElementById('navBtn');
const span = document.getElementsByClassName('close')[0];

btn.onclick = function () {
    modal.style.display = 'block';
    formID.readOnly = false;
    isEdit = false;
};

span.onclick = function () {
    modal.style.display = 'none';
};

window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = 'none';
    }
};

///form///

document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('myForm').addEventListener('submit', handleForm);
});

async function handleForm(ev) {
    ev.preventDefault();
    let myForm = ev.target;
    let data = new FormData(myForm);

    for (let key of data.keys()) {
        console.log(key, data.get(key));
    }
    let json = await dataToJson(data);

    modal.style.display = 'none';
    if (isEdit === true) {
        axiosEdit(json);
    } else {
        axiosPost(json);
    }
}

function dataToJson(data) {
    let obj = {};
    for (let key of data.keys()) {
        obj[key] = data.get(key);
    }
    console.log(obj);
    return obj;
}

///HTML///

function tableHtml(responseData) {
    for (let object of responseData) {
        let body = document.createElement('tr');
        console.log(object);

        let id = document.createElement('td');
        id.innerHTML = object.id;

        let number = document.createElement('td');
        number.innerHTML = object.number;

        let text = document.createElement('td');
        text.innerHTML = object.text;

        let date = document.createElement('td');
        date.innerHTML = object.date;

        let action = document.createElement('td');

        let btnEdit = document.createElement('button');
        btnEdit.className = 'btnEdit';
        btnEdit.innerHTML = 'Edit';
        btnEdit.onclick = function () {
            Edit(object.id, object.number, object.text, object.date);
        };

        let btnDelete = document.createElement('button');
        btnDelete.className = 'btnDelete';
        btnDelete.innerHTML = 'Delete';
        btnDelete.onclick = function () {
            Delete(object.id);
        };

        action.append(btnEdit, btnDelete);
        body.append(id, number, text, date, action);
        table.append(body);
    }
}

function Edit(id, number, text, date) {
    console.log(id, number, text, date);
    formID.readOnly = 'true';
    isEdit = true;
    formID.value = `${id}`;
    formNum.value = `${number}`;
    formText.value = `${text}`;
    formDate.value = `${date}`;
    modal.style.display = 'block';
}

///axios///

function axiosPost(json) {
    axios
        .post('/api/phone', json, {
            headers: {
                'Content-Type': 'application/json',
            },
        })
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        });
}

axios.get('/api/phone').then(response => {
    let responseData = response.data;
    tableHtml(responseData);
});

function Delete(id) {
    console.log(id);
    axios.delete(`/api/phone/${id}`);
}

function axiosEdit(json) {
    axios
        .put('/api/phone', json, {
            headers: {
                'Content-Type': 'application/json',
            },
        })
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        });
}
