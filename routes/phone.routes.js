const { Router } = require('express');

const router = new Router();

const phoneController = require('../controller/phone.controller');
router.post('/phone', phoneController.createPhone);
router.get('/phone/:id', phoneController.getOnePhone);
router.get('/phone', phoneController.getPhones);
router.put('/phone', phoneController.updatePhone);
router.delete('/phone/:id', phoneController.deletePhone);

module.exports = router;
